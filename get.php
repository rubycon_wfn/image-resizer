<?php

require __DIR__ . '/../vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\Exception\NotReadableException;

$request = Illuminate\Http\Request::capture();

$imagePath = $request->path();

if(!file_exists($imagePath)) {
    $resizedPath = $imagePath;
    $imagePath = explode('.', $imagePath);
    $ext = array_pop($imagePath);
    $filename = array_pop($imagePath);
    $filename = explode('-', $filename);
    $resizeDim = array_pop($filename);
    $resizeDim = explode('x', $resizeDim);

    array_push($imagePath, implode('-', $filename), $ext);
    $origFilepath = implode('.', $imagePath);
    if(!file_exists($origFilepath)) {
        exit;
    }

    if(count($resizeDim) != 2) {
        exit;
    }

    try {
        $image = Image::make($origFilepath);
        $image->fit($resizeDim[0], $resizeDim[1]);
        $image->save($resizedPath);
        $image->response()->send();
    } catch(NotReadableException $e) {
        exit;
    }
}