<?php
if (!function_exists('resize')) {
    function resize($url, $width, $height = false) {
        $height = $height ?: $width;
        $url = explode('.', $url);
        $url[count($url) - 2] .= '-' . $width . 'x' . $height;
        return implode('.', $url);
    }
}